import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Amne {

	
	public static void checkWindows(int n, int k, int[] prices){
		
		int previous;
		int incCount = 0, decCount = 0, windowResult = 0;
		
		//For each window
		for(int w = 0; w < n-k+1; w++){
			previous = prices[w];
			for(int i = w+1; i < w+k; i++){
				if(prices[i] > previous){
					//increase
					incCount ++;
					windowResult += incCount;					
					decCount = 0;
				}
				if(prices[i] < previous){
					//decrease
					decCount ++;
					windowResult -= decCount;				
					incCount = 0;
				}
				previous = prices[i];
			}
			
			System.out.println(windowResult);
			incCount = 0;
			decCount = 0;
			windowResult = 0;			
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException{
		                               		
		try(BufferedReader br = new BufferedReader(new FileReader(args[0]))) {		    
		    
		    String[] input;
		    
		    //Read N and K
		    String line = br.readLine();
	    	input = line.split(" ");
	    	if(input.length < 2){
	    		System.out.println("Malformed line [1]");
	    		return;
	    	}
		    	
	    	int n = Integer.parseInt(input[0]);
	    	int k = Integer.parseInt(input[1]);
	    	
	    	//Read prices
	    	line = br.readLine();
	    	input = line.split(" ");
	    	if(input.length < k){
	    		System.out.println("Malformed line [2]");
	    		return;
	    	}
	    	int[] prices = new int[input.length];
	    	for(int i = 0; i < input.length; i++){
	    		prices[i] = Integer.parseInt(input[i]);
	    	}
	    	
	    	checkWindows(n, k, prices);
	    	
		}	
		
	}
	
}
