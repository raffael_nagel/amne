# Amne Challenge #1 #
## Author: Raffael Nagel  raffael.nagel@gmail.com

This code is a solution for Amne's coding challenge

The solution consists of two parts:

## 1- Reading Input File
To read the input file I'm using a Java BufferedReader. 
The first line must contain 2 separated numbers N and K, and the second line must contain at least K space-separated numbers 

## 2- Checking subranges
To check the number of subranges I created a function that receives as parameters: 
a number N of values to check, 
a window's size K, and
an array of integers representing the prices.

A double for loop, in which the outer one goes from 0 to n-k+1 (Total number of windows) and the inner one goes from 
w(first window element) to w+k(window size), is used to check each element in each window.

For each element we check if its value is greater or smaller than the previous one, if the number is greater we add 1 plus the size of the current subrange to the total subranges counter and if the number is smaller I'm subtracting 1 plus the size of the current subrange. Also, every time the current subrange direction (increase/decrease) is equal to the previous one, the respective increase/decrease counter is incremented.
 
 
## example:

188930 194123 201345 154243 154243

194123 > 188930 so we add 1 + increaseCounter{0} to the subrange total counter
 increaseCounter ++  // we had an increase so let's increment its counter
 
201345 > 194123 so we add 1 + increaseCounter{1}

This logic accounts for all sizes of subranges. Imagine a 4 size window with only increasing values
188930 194123 201345 224312
In this case we will have the following scenario:

188930 194123 201345 224312
         +1     +2    +3     = 6
         
 The sum 6 is accounting for the subranges:
 1: 188930 194123
 2: 194123 201345
 3: 201345 224312
 4: 188930 194123 201345
 5: 194123 201345 224312
 6: 188930 194123 201345 224312
 
         
Finally, we print to the console each window subrange total


# Running

To run this app use the following command line
java -jar amne.jar input.txt